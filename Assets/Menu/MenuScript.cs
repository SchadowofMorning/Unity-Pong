﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuScript : MonoBehaviour {

	// Use this for initialization
	public void StartGame() {
		SceneManager.LoadScene("Main");
	}
	public void EndGame(){
		Debug.Log("Game Closed");
		Application.Quit();
	}
	public void StartAI() {
		SceneManager.LoadScene("AI");
	}
}
