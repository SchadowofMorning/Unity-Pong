﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour {
	GameObject Ball;
	float speed = 0.01F;
	// Use this for initialization
	void Start () {
		Ball = GameObject.Find("Circle");
	}
	void Move (float value) {
		transform.position = new Vector3(transform.position.x, transform.position.y + (value));
	}
	// Update is called once per frame
	void Update () {
		float by = Ball.transform.position.y;
		if(by > transform.position.y + 0.5F){
			Move(speed);
		}
		if(by < transform.position.y - 0.5F){
			Move(-speed);
		}
	}
}
