﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement_A : MonoBehaviour {
	GameObject Rules;
	rulesscript ruleset;
	// Use this for initialization
	float Paddlerange = 3.5F;
	
	void Start () {
		Rules = GameObject.Find("Rules");
		ruleset = Rules.GetComponent<rulesscript>();
	}

	// Update is called once per frame
	void Move (float value) {
		transform.position = new Vector3(transform.position.x, transform.position.y + (value));
	}
	void Update () {
		
		if(Input.GetKey(KeyCode.W)){
			if(transform.position.y <= Paddlerange){
				Move(ruleset.speed);
			}
		}
		if(Input.GetKey(KeyCode.S)){
			if(transform.position.y >= -Paddlerange){
				Move(-ruleset.speed);
			}
		}
	}
}
