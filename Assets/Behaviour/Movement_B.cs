﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Movement_B : MonoBehaviour {
	GameObject Rules;
	rulesscript ruleset;
	float Paddlerange = 3.5F;
	// Use this for initialization
	void Start () {
		Rules = GameObject.Find("Rules");
		ruleset = Rules.GetComponent<rulesscript>();
	}
	void Move (float value) {
		transform.position = new Vector3(transform.position.x, transform.position.y + (value));
	}
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.UpArrow)){
			if(transform.position.y <= Paddlerange){
				Move(0.2F);
			}
		}
		if(Input.GetKey(KeyCode.DownArrow)){
			if(transform.position.y >= -Paddlerange){
				Move(-0.2F);
			}
		}
	}
}
