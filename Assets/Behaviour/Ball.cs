﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Ball : MonoBehaviour {
	bool direc;
	bool go = false;
	float ymod = 0.0F;
	float speed = 0.01F;
	float BallRange = 4.25F;
	float modrange = 0.05F;
	GameObject Score_A;
	GameObject Score_B;
	GameObject Paddlehit;
	// Use this for initialization
	void Start () {
		Score_A = GameObject.Find("Score_A");
		Score_B = GameObject.Find("Score_B");
		Paddlehit = GameObject.Find("PaddleHit");
		ResetPos(Random.value == 0);
	}
	void Move (float value) {
		if(ymod >= modrange){
			ymod = modrange;
		}
		if(ymod <= -modrange){
			ymod = -modrange;
		}
		transform.position = new Vector3(transform.position.x + (value), transform.position.y + ymod);
		if(transform.position.y <= -BallRange || transform.position.y >= BallRange){
			ymod = -ymod;
		}
	}
	
	void ResetPos(bool side){
		transform.position = new Vector3(0, 0);
		ymod = 0.0F;
		speed = 0.01F;
		go = false;
		if(side == false){
			Score_A.GetComponent<Score>().increment();
			Debug.Log("Hitted A");
		}
		if(side == true){
			Score_B.GetComponent<Score>().increment();
			Debug.Log("Hitted B");
		}
		AudioSource audio = GetComponent<AudioSource>();
		audio.Play();
	}
	void playHit(){
		AudioSource hit = Paddlehit.GetComponent<AudioSource>();
		hit.Play();
	}
	void BallHit(string Stickname, bool side){
		double paddlesize = 1;
		double deadzone = 0.25;
		float increase = 0.005F;
		GameObject Stick = GameObject.Find(Stickname);
		float by = transform.position.y;
		float sy = Stick.transform.position.y;
		if(by >= sy + deadzone && by <= sy + paddlesize){
			direc = side;
			ymod = by-sy;
			playHit();
		}else if(by <= sy - deadzone && by >= sy - paddlesize){
			direc = side;
			ymod = by-sy;
			playHit();
		} else if(by >= sy - deadzone && by <= sy + deadzone){
			direc = side;
			ymod = 0.0F;
			playHit();
		} else {
			ResetPos(side);
		}
	}
	// Update is called once per frame
	void Update () {
		
		if(transform.position.x <= -4.5F){
			BallHit("Stick A", true);
		}
		if(transform.position.x >= 4.5F){
			BallHit("Stick B", false);
		}
		if(Input.GetKey(KeyCode.Space)){
			go = true;
		}
		if(go == true){
			if(direc == false){
				Move(-0.1F+ speed);
			}
			if(direc == true){
				Move(0.1F + speed);
			}
		}
		
		
	}
}
